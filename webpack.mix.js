const mix = require('laravel-mix');

mix.sass('resources/sass/app.scss', 'public/css')
    .options({
        autoprefixer: {
            options: {
                browsers: [
                    'last 10 versions',
                ]
            }
        }
    })


