<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\User;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_it_shows_warning_message_and_not_submit_form()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
            ->visit('/admin/artikel/pilihlokasi')
              ->press('Pilih Lokasi')
              ->waitForDialog($seconds = null)
              ->assertDialogOpened("harap pilih lokasi anda");
        });
    }
    public function test_it_shows_error_message_on_invalid_location()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
            ->visit('/admin/artikel/pilihlokasi')

            ->click('#map')
          ->moveByOffset(100, 100)
                ->press('Pilih Lokasi')
              ->waitForDialog($seconds = null)
              ->assertDialogOpened("area bukan kewenangan BPBD DIY");
        });
    }
}
