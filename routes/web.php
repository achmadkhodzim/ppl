<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//landing page

Route::get('/', 'LandingController@index');

Route::get('/home/{id}', 'LandingController@indexfiltered');

Route::get('/kontak', 'LandingController@index');
Route::get('/kontak', function () {
    return view('tradisi.kontak');
});

Route::get('/sambutan', function () {
    return view('tradisi.sambutan');
});

Route::get('/profil', function () {
    return view('tradisi.profil');
});
Route::get('/artikel/{slug}', 'LandingController@article');
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login', 'AuthController@loginPost');
Route::post('/logout', 'AuthController@getLogout')->name('logout');

Route::get('/warung-rakyat', 'LandingController@allWarung');
Route::get('/detail-warung/{id}', 'LandingController@detailWarung');
Route::get('/semua-artikel', 'LandingController@allArticle');




//admin page

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', 'LandingController@dashboard');

    Route::get('/kecamatan/{id}', 'ArticlesController@dataKecamatan');

    Route::get('/kelurahan/{id}', 'ArticlesController@dataKelurahan');
    Route::get('/artikel/pilihlokasi', 'ArticlesController@picklocation');
    Route::get('/accept/{id}', 'ArticlesController@accept');
    Route::get('/deny/{id}', 'ArticlesController@deny');

    Route::get('/getpostcode/{id}', 'ArticlesController@getkodepos');
    Route::get('/kodepos/{id}', 'ArticlesController@getpostcode');
    Route::resource('slider', 'SlideController');
    Route::resource('artikel', 'ArticlesController');
    Route::resource('tag', 'TagController');
    Route::resource('warung-rakyat', 'WarungController');
});
