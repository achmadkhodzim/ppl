<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Kecamatan;

class CreateKecamatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kecamatans', function (Blueprint $table) {
            $table->id();
            $table->string('kecamatan');
            $table->integer('kelurahan_id');

            $table->string('kodepos');
            $table->timestamps();
        });
        Kecamatan::create(['kecamatan' => 'Kecamatan1','kelurahan_id' => '1','kodepos'=>'55151']);
        Kecamatan::create(['kecamatan' => 'Kecamatan2','kelurahan_id' => '1','kodepos'=>'55664']);
        Kecamatan::create(['kecamatan' => 'Kecamatan3','kelurahan_id' => '1','kodepos'=>'55584']);
        Kecamatan::create(['kecamatan' => 'Kecamatan4','kelurahan_id' => '1','kodepos'=>'55185']);
        Kecamatan::create(['kecamatan' => 'Kecamatan5','kelurahan_id' => '1','kodepos'=>'55152']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kecamatans');
    }
}
