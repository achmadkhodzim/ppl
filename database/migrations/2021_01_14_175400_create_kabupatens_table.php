<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Kabupaten;

class CreateKabupatensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kabupatens', function (Blueprint $table) {
            $table->id();
            $table->string('kabupaten');
            $table->timestamps();
        });
        Kabupaten::create(['kabupaten' => '	Kabupaten Bantul']);
        Kabupaten::create(['kabupaten' => '	Kabupaten Gunungkidul']);
        Kabupaten::create(['kabupaten' => '	Kabupaten Kulon Progo']);
        Kabupaten::create(['kabupaten' => '	Kabupaten Sleman']);
        Kabupaten::create(['kabupaten' => 'Kota Yogyakarta']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kabupatens');
    }
}
