<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Kelurahan;

class CreateKelurahansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelurahans', function (Blueprint $table) {
            $table->id();
            $table->string('kelurahan');
            $table->integer('kabupaten_id');
            $table->timestamps();
        });
        Kelurahan::create(['kelurahan' => 'Bambanglipuro','kabupaten_id' => '1']);
        Kelurahan::create(['kelurahan' => 'Banguntapan','kabupaten_id' => '1']);
        Kelurahan::create(['kelurahan' => 'Bantul','kabupaten_id' => '1']);
        Kelurahan::create(['kelurahan' => 'Dlingo','kabupaten_id' => '1']);
        Kelurahan::create(['kelurahan' => 'Imogiri','kabupaten_id' => '1']);
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelurahans');
    }
}
