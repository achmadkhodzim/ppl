<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;
use App\Kabupaten;

use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dataKelurahan($id)
    {
        // $data['data']=[];
        $kelurahan=Kelurahan::where('kabupaten_id', $id)->get();

        //
        //   foreach ($kelurahan as $kelurahan) {
        //       array_push($data['data'], array(
        //   'id' => $kelurahan->id,
        //   'kelurahan' => $kelurahan->kelurahan,
        // ));
        //   }
        return response()->json($kelurahan);
    }

    public function dataKecamatan($id)
    {
        // $data['data']=[];
        $kelurahan=Kecamatan::where('kelurahan_id', $id)->get();

        //
        //   foreach ($kelurahan as $kelurahan) {
        //       array_push($data['data'], array(
        //   'id' => $kelurahan->id,
        //   'kelurahan' => $kelurahan->kelurahan,
        // ));
        //   }
        return response()->json($kelurahan);
    }
    public function getkodepos($id)
    {
        // $data['data']=[];
        $kelurahan=Kecamatan::where('id', $id)->first();

        //
        //   foreach ($kelurahan as $kelurahan) {
        //       array_push($data['data'], array(
        //   'id' => $kelurahan->id,
        //   'kelurahan' => $kelurahan->kelurahan,
        // ));
        //   }

        return response()->json($kelurahan->kodepos);
    }
    public function getpostcode($id)
    {
        // $data['data']=[];
        $kelurahan=Kecamatan::where('kodepos', $id)->get();

        //
        //   foreach ($kelurahan as $kelurahan) {
        //       array_push($data['data'], array(
        //   'id' => $kelurahan->id,
        //   'kelurahan' => $kelurahan->kelurahan,
        // ));
        //   }
        if (count($kelurahan)>0) {
            return response()->json('true');
        }
        return response()->json('false');
    }
    public function index()
    {
        $articles = Article::all();
        return view('admin.artikel.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kabupatens = Kabupaten::all();
        return view('admin.artikel.create1', compact('kabupatens'));
    }
    public function accept($id)
    {
        $articles = Article::where('id', $id)->first();
        $articles->status=1;
        $articles->update();

        return redirect()->back();
    }
    public function deny($id)
    {
        $articles = Article::where('id', $id)->first();
        $articles->status=0;
        $articles->update();

        return redirect()->back();
    }
    public function picklocation()
    {
        return view('admin.artikel.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kecamatanname=Kecamatan::where('id', $request->kecamatan)->first();

        if ($request->hasFile('thumbnail123')) {
            $resorce = $request->file('thumbnail123');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);
        }

        $artikel = Article::create([
            'judul' => $request->jenis." ".$kecamatanname->kecamatan,
            'isi' => $request->isi,
            'slider' => false,

                        'jenis' => $request->jenis,
                        'kabupaten_id' => $request->kabupaten,
            'kecamatan_id' => $request->kecamatan,
            'slug' => Str::slug($request->jenis." ".$kecamatanname->kecamatan." ".Carbon::now()),
            'thumbnail' => '/uploads/'.$name,
          ]);


        return redirect('/admin/artikel')->with('success', 'Tambah Artikel Berhasil !');
    }

    /**

     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $artikel)
    {
        $tags = Tag::all();
        return view('admin.artikel.edit', compact('tags', 'artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $artikel)
    {
        if ($request->hasFile('thumbnail')) {
            $resorce = $request->file('thumbnail');
            $name = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/uploads", $name);

            $post = [
              'judul' => $request->judul,
              'isi' => $request->isi,
              'slider' => false,
                'kabupaten_id' => $request->kabupaten,
              'slug' => Str::slug($request->judul),
              'thumbnail' => '/uploads/'.$name,
            ];
        } else {
            $post = [
              'judul' => $request->judul,
              'isi' => $request->isi,
              'slider' => false,
                'kabupaten_id' => $request->kabupaten,
              'slug' => Str::slug($request->judul),
            ];
        }




        $artikel->update($post);

        return redirect('/admin/artikel')->with('success', 'Edit Artikel Berhasil !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $artikel)
    {
        $artikel->delete();
        return redirect()->back()->with('success', 'Hapus Artikel Berhasil !');
    }
}
