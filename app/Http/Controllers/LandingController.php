<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Slide;
use App\Tag;

use App\Kabupaten;

class LandingController extends Controller
{
    public function index()
    {
        $tagfooter = Tag::paginate(5);
        $slider = Slide::latest()->get();
        $articles = Article::latest()->paginate(4);
        $kabupaten = Kabupaten::all();
        $current="all";
        // $slider = Article::where('slider', '=', 'true')->get();
        return view('tradisi.index', compact('current', 'kabupaten', 'tagfooter', 'articles', 'slider'));
    }
    public function indexfiltered($id)
    {
        if ($id=="all") {
            $articles = Article::latest()->paginate(4);
        } else {
            $articles = Article::where('kabupaten_id', $id)->latest()->paginate(4);
        }
        $tagfooter = Tag::paginate(5);
        $slider = Slide::latest()->get();
        $current=$id;
        $kabupaten = Kabupaten::all();
        // $slider = Article::where('slider', '=', 'true')->get();
        return view('tradisi.index', compact('current', 'kabupaten', 'tagfooter', 'articles', 'slider'));
    }
    public function dashboard()
    {
        $articles = Article::all()->count();
        return view('admin.index', compact('articles'));
    }
    public function article($slug)
    {
        $tagfooter = Tag::paginate(5);

        $artikel = Article::where('slug', '=', $slug)->first();
        Article::where('slug', '=', $slug)->increment('view_count', 1);
        $recent = Article::latest()->paginate(4);
        $tag = Tag::all();
        return view('tradisi.article', compact('tagfooter', 'recent', 'tag', 'artikel'));
    }



    public function allArticle(Request $request)
    {
        $tagfooter = Tag::paginate(5);

        $search = null;
        $tag = null;
        if ($request->has('search')) {
            $search = $request->search;
            $articles = Article::where('judul', 'LIKE', '%'.$search.'%')->paginate(5);
        } elseif ($request->has('tag')) {
            $tag = Tag::where('nama', $request->tag)->first();
            $articles = $tag->article()->paginate(5);
        } else {
            $articles = Article::paginate(5);
        }
        return view('landing.semua-artikel', compact('tagfooter', 'articles', 'search', 'tag'));
    }
}
