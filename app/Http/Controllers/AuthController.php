<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Socialite;

class AuthController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/masuk');
        }
        // only allow people with @company.com to login
        if (explode("@", $user->email)[1] !== 'uii.ac.id') {
            return redirect('/masuk')->with('failed', 'Login Hanya Menggunakan Email UII!');
        }
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if ($existingUser->role=='fakultas') {
            return redirect('/masuk/staff')->with('error', 'Dashboard Fakultas Masih ada kendala!');
        }
        if ($existingUser) {
            // log them in
            if ($existingUser->google_id==null) {
                $existingUser ->nama           = $user->name;
                $existingUser->email           = $user->email;
                $existingUser->google_id       = $user->id;
                $existingUser->avatar          = $user->avatar;
                $existingUser->avatar_original = $user->avatar_original;
                $existingUser->update();
                Auth::login($existingUser, true);
            } else {
                Auth::login($existingUser, true);
            }
        } else {
            return redirect('/masuk')->with('failed', 'Maaf, Email Anda Tidak Terdaftar Mohon Segera Hubungi Admin');
        }
        if ($existingUser->role=='admin') {
            return redirect('/admin/aktivasi')->with('success', 'Login Berhasil');
        }
        if ($existingUser->role=='dosen') {
            return redirect('/simkid/dashboard')->with('success', 'Login Berhasil');
        }
        if ($existingUser->role=='jurusan') {
            return redirect('/jurusan/dashboard')->with('success', 'Login Berhasil');
        }
    }
    public function login()
    {
        return view('login');
    }
    public function daftar()
    {
        $users = User::all();
        $unit = unit::all();
        return view('pages.direktur.administrator', compact('users', 'unit'));
    }
    // public function validation($request){
    // return $this->validate($request, [
    //   'nama' => 'required|max:255',
    //   'role_id' => 'required|max:255',
    //   'unit_id' => 'required|max:255',
    //   'username' => 'required|max:255|unique:users',
    //   'password' => 'required|min:5|max:255',
    // ]);
    // }

    public function daftarkan(Request $request)
    {
        // $this->validation($request);
        $request['password'] = bcrypt($request->password);
        $daftar = new User;
        $daftar->nama = 'admin';
        $daftar->nip = 123123;
        $daftar->role = 'admin';
        $daftar->email = 'asd@sad.com';
        $daftar->foto = 'asd@sad.com';

        $daftar->jurusan_id = 1;
        $daftar->password = $request->password;
        $daftar -> save();
        return redirect()->back()->with('success', 'Daftar Sukses !');
    }

    public function loginPost(Request $request)
    {
        $this -> validate($request, [
      'email' => 'required|max:255',
      'password' => 'required|max:255',
    ], ['required' => 'Pastikan Email dan Password Terisi dengan benar']);
        if (Auth::attempt(['email' => $request -> email,'password' => $request -> password])) {
            return redirect('/admin/dashboard');
        }
        // if (Auth::attempt(['nip' => $request -> email,'password' => $request -> password, 'role' => 'dosen'], true)) {
        //     return redirect('/simkid/dashboard');
        // }
        // if (Auth::attempt(['nip' => $request -> email,'password' => $request -> password, 'role' => 'jurusan'])) {
        //     return redirect('/jurusan/dashboard');
        // }
        // if (Auth::attempt(['nip' => $request -> username,'password' => $request -> password, 'role' => 'fakultas'])) {
        //     Auth::logout();
        //     Session::flush();

        //     return redirect('/masuk/staff')->with('error', 'Dashboard Fakultas Masih ada kendala!');
        // }
        return redirect()->back()->with('failed', 'Pastikan Username dan Password Terisi dengan benar');
    }
    public function getLogout()
    {
        Auth::logout();
        Session::flush();
        return Redirect('/');
    }
    public function delete($id)
    {
        $user = user::where('id', $id)->first();
        $user->delete();
        return redirect()->back()->with('success', 'Data berhasil di hapus');
    }
}
