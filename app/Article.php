<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function tag()
    {
        return $this->belongsToMany('App\Tag');
    }   
}
