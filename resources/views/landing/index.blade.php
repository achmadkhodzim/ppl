@extends('layouts.index')
@section('content')
<section id="hero" class="home-slider owl-carousel">
    @foreach($slider as $slide)

    <div class="slider-item">
        <div class="container">
            <div
                class="row d-flex slider-text justify-content-center align-items-center"
                data-scrollax-parent="true"
            >
                <div
                    class="img"
                    style="background-image: url({{ $slide -> thumbnail }});"
                ></div>

                <div class="text d-flex align-items-center ftco-animate">
                    <div class="text-2 pb-lg-5 mb-lg-4 px-4 px-md-5">
                        <h3 class="subheading mb-3">Featured Posts</h3>
                        <h1 class="mb-5">
                            {{ $slide -> judul }}
                        </h1>
                        <p class="mb-md-5">
                        @php
                            $isi = strip_tags($slide->isi);
                        @endphp
                        {{ Str::words($isi, $words = 10, $end = '...') }}
                        </p>
                        <p>
                            <a href="{{ '/artikel/'. $slide -> slug }}" class="btn btn-black px-3 px-md-4 py-3"
                                >Read More
                                <span class="icon-arrow_forward ml-lg-4"></span
                            ></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endforeach
</section>

<section class="ftco-counter ftco-section ftco-no-pb" id="section-counter">
    <div class="container">
        <div class="row d-flex">
            <div class="col-md-4 d-flex">
                <div class="sidebar-wrap pt-4" style="background-color:#FFF">
                    <div
                        class="sidebar-box categories text-center ftco-animate"
                    >
                        {{-- <h2 class="heading mb-4">Pranala Penting</h2> --}}
                        <ul class="category-image">
                        <li>
                                <a
                                    href="https://corona.kutaitimurkab.go.id"
                                    class="img d-flex align-items-center justify-content-center text-center"
                                    style="background-image: url(images/covid.png);"
                                    target="_blank"
                                >
                                </a>
                            </li>
                            <li>
                                <a
                                    href="https://data.kutaitimurkab.go.id"
                                    class="img d-flex align-items-center justify-content-center text-center"
                                    style="background-image: url(images/satu.png);"
                                    target="_blank"
                                >
                                </a>
                            </li>
                             <li>
                                <a
                                    href="http://ppid.kutaitimurkab.go.id/"
                                    class="img d-flex align-items-center justify-content-center text-center"
                                    style="background-image: url(images/ppid.png);"
                                    target="_blank"
                                >
                                </a>
                            </li>
                            
                            <li>
                                <a
                                    href="http://www.dinkeskutim.net/"
                                    class="img d-flex align-items-center justify-content-center text-center"
                                    style="background-image: url(images/dinas.png);"
                                    target="_blank"
                                >
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 pl-md-5 py-5">
                <div class="row justify-content-start pb-3">
                    <div class="col-md-12 heading-section ftco-animate">
                        <h2 class="mb-4">Jumlah kasus di Kutai Timur</h2>
                        <!-- <p>
                            Berikut adalah jumlah kasus di Kabupaten Kutai Timur
                            per hari ini.
                        </p> -->
                    </div>
                </div>
                <div class="row" style="max-height:600px;">
                    

                <div class='tableauPlaceholder' id='viz1597781392785' style='position: relative'><noscript><a href='https:&#47;&#47;corona.kutaitimurkab.go.id&#47;'><img alt=' ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;ne&#47;newcovid_15955025099650&#47;BERANDA&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='newcovid_15955025099650&#47;BERANDA' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;ne&#47;newcovid_15955025099650&#47;BERANDA&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='language' value='en' /></object></div>                

                </div>
            </div>
        </div>
    </div>
</section>

<section id="artikel-terbaru" class="ftco-section ftco-no-pb bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-7 heading-section ftco-animate">
                <h2 class="mb-4"><span>Artikel Terbaru</span></h2>
            </div>
        </div>
        <div class="row">
            @foreach($articles->slice(0,1) as $article)

            <div class="col-md-6 order-md-last col-lg-6 ftco-animate">
                <div class="blog-entry">
                    <div
                        class="img img-big d-flex align-items-end"
                        style="background-image: url({{ $article -> thumbnail }});"
                    >
                        <div class="overlay"></div>
                        <div class="text">
                            <span class="subheading">  @foreach( $article -> tag as $tag)
                                        {{ $tag ->nama }}
                                        @endforeach</span>
                            <h3><a href="#">{{ $article -> judul }}</a></h3>
                            <p class="mb-0">
                                <a href="{{ '/artikel/'. $article -> slug }}" class="btn-custom"
                                    >Read More
                                    <span class="icon-arrow_forward ml-4"></span
                                ></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-md-6">
                <div class="row">
                    @foreach($articles->slice(1,5) as $article)

                    <div class="col-md-6 ftco-animate">
                        <div class="blog-entry">
                            <a
                                href="#"
                                class="img d-flex align-items-end"
                                style="background-image: url({{ $article -> thumbnail }});"
                            >
                                <div class="overlay"></div>
                            </a>
                            <div class="text pt-3">
                                <p class="meta d-flex">
                                    <span class="pr-3">
                                        @foreach( $article -> tag as $tag)
                                        {{ $tag ->nama }}
                                        @endforeach

                                    </span>
                                    <span class="ml-auto pl-3"
                                        >{{ date('d M Y', strtotime($article->created_at))  }}</span
                                    >
                                </p>
                                <h3>
                                    <a href="{{ '/artikel/'. $article -> slug }}">{{ $article -> judul }}</a>
                                </h3>
                                <p class="mb-0">
                                    <a href="{{ '/artikel/'. $article -> slug }}" class="btn-custom"
                                        >Read More
                                        <span
                                            class="icon-arrow_forward ml-4"
                                        ></span
                                    ></a>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center pb-5">
                 <p>
                     <a href="{{url('/semua-artikel')}}" class="btn-custom"
                                        >Lihat Semua Artikel
                                        <span
                                            class="icon-arrow_forward ml-4"
                                        ></span
                                    ></a>
                 </p>
            </div>
        </div>
    </div>
</section>

<section id="kasus-indonesia" class="ftco-counter ftco-section ftco-no-pt img" id="section-counter">
    <div class="container">
        <div class="row d-flex">
            <div class="col-md-6 d-flex">
                <div
                    class="img d-flex align-self-stretch"
                    style="background-image:url(images/lindungisesama.svg);"
                ></div>
            </div>
            <div class="col-md-6 pl-md-5 py-5">
                <div class="row justify-content-start pb-3">
                    <div class="col-md-12 heading-section ftco-animate">
                        <h2 class="mb-4">Jumlah kasus di Indonesia</h2>
                        <p>
                            Berikut adalah jumlah kasus di Indonesia per hari
                            ini.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div
                        class="col-md-6 justify-content-center counter-wrap ftco-animate"
                    >
                        <div class="block-18 text-center py-5 bg-light mb-4">
                            <div class="text">
                                <strong class="number" id="indonesia-confirmed" data-number="0"
                                    >0</strong
                                >
                                <span>Terkonfirmasi</span>
                            </div>
                        </div>
                    </div>
                    <div
                        class="col-md-6 justify-content-center counter-wrap ftco-animate"
                    >
                        <div class="block-18 text-center py-5 bg-light mb-4">
                            <div class="text">
                                <strong class="number" id="indonesia-perawatan"  data-number="0"
                                    >0</strong
                                >
                                <span>Dalam Perawatan</span>
                            </div>
                        </div>
                    </div>
                    <div
                        class="col-md-6 justify-content-center counter-wrap ftco-animate"
                    >
                        <div class="block-18 text-center py-5 bg-light mb-4">
                            <div class="text">
                                <strong class="number" id="indonesia-sembuh"  data-number="0"
                                    >0</strong
                                >
                                <span>Sembuh</span>
                            </div>
                        </div>
                    </div>
                    <div
                        class="col-md-6 justify-content-center counter-wrap ftco-animate"
                    >
                        <div class="block-18 text-center py-5 bg-light mb-4">
                            <div class="text">
                                <strong class="number" id="indonesia-meninggal"  data-number="0"
                                    >0</strong
                                >
                                <span>Meninggal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    {{-- <section class="ftco-section ftco-no-pt">
    	<div class="container">
        <div class="row">
        	<div class="col-lg-12">
        		<div class="row">
		          <div class="col-md-12 heading-section ftco-animate">
		            <h2 class="mb-4"><span>Featured Posts</span></h2>
		          </div>
		        </div>
        		<div class="row">
        			<div class="col-md-3 ftco-animate">
    						<div class="blog-entry">
		    					<a href="single.html" class="img-2"><img src="images/blog-1.jpg" class="img-fluid" alt="Colorlib Template"></a>
		    				</div>
    					</div>
        		</div>
        	</div>
        </div>
    	</div>
    </section> --}}

{{--
<section class="ftco-subscribe ftco-section bg-light">
    <div class="overlay">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div
                    class="col-md-8 text-wrap text-center heading-section ftco-animate"
                >
                    <h2 class="mb-4">
                        <span>Subcribe to our Newsletter</span>
                    </h2>
                    <p>
                        A small river named Duden flows by their place and
                        supplies it with the necessary regelialia. It is a
                        paradisematic country, in which roasted parts of
                        sentences fly into your mouth.
                    </p>
                    <div class="row d-flex justify-content-center mt-4 mb-4">
                        <div class="col-md-8">
                            <form action="#" class="subscribe-form">
                                <div class="form-group d-flex">
                                    <input
                                        type="text"
                                        class="form-control"
                                        placeholder="Enter email address"
                                    />
                                    <input
                                        type="submit"
                                        value="Subscribe"
                                        class="submit px-3"
                                    />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@endsection


@push('javascript')
<script>
    $(document).ready(function(){
        $.ajax({
            url: "https://indonesia-covid-19.mathdro.id/api",
            method: "GET",
            success: function(res) {
                $('#indonesia-confirmed')
                    .attr('data-number', res.jumlahKasus)
                    .html(res.jumlahKasus)

                $('#indonesia-perawatan')
                    .attr('data-number', res.perawatan)
                    .html(res.perawatan)

                $('#indonesia-sembuh')
                    .attr('data-number', res.sembuh)
                    .html(res.sembuh)

                $('#indonesia-meninggal')
                    .attr('data-number', res.meninggal)
                    .html(res.meninggal)
            }
        });
    })

    

</script>
<script type='text/javascript'>                    var divElement = document.getElementById('viz1597781392785');                    var vizElement = divElement.getElementsByTagName('object')[0];                    if ( divElement.offsetWidth > 800 ) { vizElement.style.width='100%';vizElement.style.height=(divElement.offsetWidth*0.75)+'px';} else if ( divElement.offsetWidth > 500 ) { vizElement.style.width='100%';vizElement.style.height=(divElement.offsetWidth*0.75)+'px';} else { vizElement.style.width='100%';vizElement.style.height='1027px';}                     var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);                </script>
@endpush
