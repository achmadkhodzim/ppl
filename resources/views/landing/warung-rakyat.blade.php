@extends('layouts.index')
@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_4.jpg');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
                <h1 class="mb-3 bread">Warung Rakyat</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i
                                class="ion-ios-arrow-forward"></i></a></span> <span>Warung Rakyat <i
                            class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>


<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pb-5">
                <form action="#" class="search-form" method="GET">
                    <div class="form-group">
                        <span class="icon icon-search"></span>
                        <input type="text" name="search" class="form-control" placeholder="Search">
                    </div>
                </form>
            </div>
            @isset($search)
            <div class="col-lg-12">
                <h2>Hasil Pencarian: {{$search}}</h2>
				<br>
				<p>Data tidak ditemukan</p>
            </div>
            @endisset
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">

                    @foreach($warungs as $warung)
                    <div class="col-md-4 ftco-animate">
                        <div class="blog-entry">
                            <a href="{{ 'detail-warung/'. $warung-> id }}" class="img-2"><img src="images/blog-3.jpg" class="img-fluid"
                                    alt="Colorlib Template"></a>
                            <div class="text pt-3">
                                <p class="meta d-flex"><span class="ml-auto pl-3">{{ $warung -> region -> name }}</span></p>
                                <h3><a href="#l">{{ $warung -> nama }}</a></h3>
                                <p class="mb-0"><a href="{{ 'detail-warung/'. $warung-> id }}" class="btn btn-black py-2">Lihat <span
                                            class="icon-arrow_forward ml-4"></span></a></p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="row mt-5">
                    <div class="col text-center">
                        <div class="block-27">
                            {{ $warungs->links() }}

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="sidebar-wrap">
                    <div class="sidebar-box p-4 about text-center ftco-animate">
                        <h2 class="heading mb-4">Tentang</h2>

                        <div class="text pt-4">
                            <p>Warung rakyat adalah portal untuk digunakan berjualan online oleh pelaku usaha mikro, kecil, dan menengah (UMKM). Langkah ini dilakukan untuk mengakomodasi ratusan pelaku UMKM sebagai penggerak ekonomi yang mulai terganggu karena pandemi Covid-19.
                              </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
