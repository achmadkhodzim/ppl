@extends('layouts.index')
@section('content')

<section
    class="hero-wrap hero-wrap-2"
    style="background-image: url({{ $warung-> photo }});"
>
    <div class="overlay"></div>
    <div class="container">
        <div
            class="row no-gutters slider-text align-items-end justify-content-center"
        >
            <div class="col-md-9 ftco-animate pb-5 text-center">
                <h1 class="mb-3 bread">{{ $warung-> nama }}</h1>
                <p class="breadcrumbs"><span>{{ $warung-> region -> name }}</span></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12 col-lg-12 ftco-animate">
                        <div class="blog-entry d-lg-flex">
                            <div class="half">
                                <a
                                    href="#"
                                    class="img d-flex align-items-end"
                                    style="background-image: url({{ $warung-> photo }});"
                                >
                                    <div class="overlay"></div>
                                </a>
                            </div>
                            <div class="text px-md-4 px-lg-5 half pt-3">

                                <h3><a href="single.html">{{ $warung-> nama }}</a></h3>
                                                           <p class="mt-2 mb-2">
                                                           
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Pemilik</td>
                                                <td>{{ $warung-> pemilik }}</td>
                                            </tr>
                                            <tr>
                                                <td>Kontak</td>
                                                <td>{{ $warung-> nopemilik }}</td>
                                            </tr>
                                            <tr>
                                                <td>Metode Pengiriman</td>
                                                <td>{{ $warung-> metode }}</td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>{{ $warung-> alamat }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                 <p class="meta d-flex mt-2">
                                                                  <span class="pr-3">Deskripsi</span>
                                </p>
                                <p>{!! $warung-> deskripsi !!}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
