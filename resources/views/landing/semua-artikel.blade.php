@extends('layouts.index')
@section('content')

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_4.jpg');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
                <h1 class="mb-3 bread">Semua Artikel</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i
                                class="ion-ios-arrow-forward"></i></a></span> <span>Semua Artikel <i
                            class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>


<section class="ftco-section">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 pb-5">
                <form action="#" class="search-form" method="GET">
                    <div class="form-group">
                        <span class="icon icon-search"></span>
                        <input type="text" class="form-control" placeholder="Search" name="search">
                    </div>
                </form>
            </div>

            @isset($search)
            <div class="col-lg-12">
                <h2>Hasil Pencarian: {{$search}}</h2>
				<br>

				@if(count($articles) == 0)
				<p>Data tidak ditemukan</p>
				@endif
            </div>
            @endisset

			@isset($tag)
            <div class="col-lg-12">
                <h2>Tag: {{$tag->nama}}</h2>
				<br>

				@if(count($articles) == 0)
				<p>Data tidak ditemukan</p>
				@endif
            </div>
            @endisset

        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="row">
                    @foreach($articles as $article)
                    <div class="col-md-6 col-lg-12 ftco-animate">
                        <div class="blog-entry d-lg-flex">
                            <div class="half">
                                <a href="single.html" class="img d-flex align-items-end"
                                    style="background-image: url({{ $article -> thumbnail }});">
                                    <div class="overlay"></div>
                                </a>
                            </div>
                            <div class="text px-md-4 px-lg-5 half pt-3">
                                <p class="meta d-flex"><span class="pr-3">@foreach( $article -> tag as $tag)
                                        {{ $tag ->nama }}
                                        @endforeach</span><span
                                        class="ml-auto pl-3">{{ date('d M Y', strtotime($article->created_at))  }}</span>
                                </p>
                                <h3><a href="single.html">{{ $article -> judul }}</a></h3>
                                <p>{!! substr($article->isi, 0, 50) !!}
                                </p>
                                <p class="mb-0"><a href="{{ '/artikel/'.$article -> slug }}"
                                        class="btn btn-primary">Read More <span
                                            class="icon-arrow_forward ml-4"></span></a></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row mt-5">
                    <div class="col text-center">
                        <div class="block-27">
                            {{ $articles->links() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection