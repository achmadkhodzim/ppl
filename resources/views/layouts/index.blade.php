<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>KKN Covid 2020</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    @stack('css')
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="/">KKN Covid<span>.</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a href="{{url('/')}}" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="{{url('/#artikel-terbaru')}}" class="nav-link">Artikel Terbaru</a></li>
                    <li class="nav-item"><a href="{{url('/#kasus-indonesia')}}" class="nav-link">Kasus diIndonesia</a></li>
                    <li class="nav-item"><a href="{{url('warung-rakyat')}}" class="nav-link">Yuk Bantu UMKM</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->
    @yield('content')

    <footer class="ftco-footer ftco-footer-2 ftco-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Tentang</h2>
                        <h3 style="color:white">Visi</h3>
                        <p>
                            Menjadi rujukan untuk New Normal di Kutai Timur
                          </p>
                          <h3 style="color:white">Misi</h3>
                            <ul>
                              <li>
                                  Membantu masyarakat untuk menyesuaikan kondisi terhadap dampak Covid-19 di Kutai Timur
                              </li>
                              <li>
                                Sebagai sarana staging, tracking, tracing dan mitigasi penanangan dampak Covid-19 di Kutai Timur
                              </li>
                              <li>
                                Membantu pemerintah dan organisasi dalam membentuk keputusan dengan cepat dan tepat
                              </li>
                            </ul>



                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <!-- <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li> -->
                            <li class="ftco-animate"><a href="https://www.instagram.com/kkn.uii.61.48.covid/"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4 ml-md-5">
                        <h2 class="ftco-heading-2">Tag</h2>
                        <ul class="list-unstyled">
                            <form action="/semua-artikel" method="GET">
                                @foreach($tagfooter as $tag)
                                <li><button style="cursor:pointer; background:none; padding:0; color:#FFFFFFB3; border:none;" type="submit" name="tag" class="py-2 d-block" value="{{$tag ->nama}}">{{$tag ->nama}}</button></li>
                                @endforeach
                            </form>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Link Penting</h2>
                        <ul class="list-unstyled">
                            <li><a href="https://data.kutaitimurkab.go.id" class="py-2 d-block">SATU DATA KUTIM</a></li>
                            <li><a href="http://ppid.kutaitimurkab.go.id/" class="py-2 d-block">PPID KUTIM</a></li>
                            <li><a href="http://www.dinkeskutim.net/" class="py-2 d-block">DINKES KUTIM</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Hubungi kami ?</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Jl. Prof. Dr. Sudiatmo (Ex. Dinas Kehutanan) Kawasan Bukit Pelangi Kab. Kutai Timur. Kode Pos. 75611</span></li>
                                <!-- <li><a href="#"><span class="icon icon-phone"></span><span class="text">Sleman</span></a></li> -->
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text"> newnormal@kutaitimurkab.go.id</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        Created by KKN Covid UII for Pemkab Kutai Timur.<br> Template by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/aos.js')}}"></script>
    <script src="{{asset('js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('js/scrollax.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

    @stack('javascript')
</body>

</html>
