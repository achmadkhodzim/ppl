@extends('layouts.admin')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif



    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat Laporan</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat Artikel
            Baru</a> -->
    </div>
    <div class="row">

      <div class="col-md-12">

           <div id="map"></div>
           <input type="hidden" id="latbox"/>
                <input type="hidden" id="lngbox"/>
                  <input type="hidden" id="kodepos"/>
                <div class="form-group">
                    <button id="button" class="btn btn-primary mb-5">Pilih Lokasi</button>
                </div>

        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection

@push('css')
<style type="text/css">
     /* Always set the map height explicitly to define the size of the div
      * element that contains the map. */
     #map {
       height: 500px;
     }

     /* Optional: Makes the sample page fill the window. */
     html,
     body {
       height: 100%;
       margin: 0;
       padding: 0;
     }
   </style>
@endpush

@push('javascript')

<script>
$("button").click(function(){
  var kodepos;
  var lokasi;
  if(document.getElementById("latbox").value==""){
    alert('harap pilih lokasi anda');
  }
  else{
    $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+document.getElementById("latbox").value+','+document.getElementById("lngbox").value+"&key=AIzaSyDHE7kFmlDnyo2YPsSpUOLJdmgvW8FwjzQ", function(data, status){
      kodepos=data.results[0].address_components[7].long_name;
      $.get("/admin/kodepos/"+kodepos, function(lokasi, status){
        console.log(lokasi);
        if(lokasi=="true"){
          alert('lokasi dalam jogja');
          window.location.href = "/admin/artikel/create";
        }
        else{
    alert('area bukan kewenangan BPBD DIY');
        }
   });
    });




  }

});
</script>
@endpush
