@extends('layouts.admin')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat Laporan</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat Artikel
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="/admin/artikel" method="POST" enctype="multipart/form-data" style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="tag">Jenis Bencana</label>
                          <select class="form-control select2" name="jenis">

                              <option value="Tanah Longsor">Tanah Longsor</option>
                              <option value="Gempa Bumi">Gempa Bumi</option>
                              <option value="Tsunami">Tsunami</option>
                              <option value="Banjir">Banjir</option>
                              <option value="Letusan Gunung Api">Letusan Gunung Api</option>
                              <option value="Angin topan">Angin topan</option>
                              <option value="Kekeringan">Kekeringan</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="tag">Provinsi</label>
                          <select class="form-control select2" name="provinsi">

                              <option value="1">Daerah Istimewa Yogyakarta</option>

                          </select>
                      </div>
                      <div class="form-group">
                          <label for="tag">Kabupaten</label>
                          <select id="city-select" class="form-control" name="kabupaten">
                              @foreach($kabupatens as $kabupaten)
                              <option value="{{ $kabupaten -> id }}">{{ $kabupaten -> kabupaten }}</option>
                              @endforeach
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="tag">Kelurahan</label>
                          <select id="kelurahan-select" class="form-control select2" name="kelurahan">



                          </select>
                      </div>
                      <div class="form-group">
                          <label for="tag">Kecamatan</label>
                          <select id="kecamatan-select" class="form-control select2" name="kecamatan">



                          </select>
                      </div>
                      <div class="form-group">
                          <label for="tag">Kode Pos</label>
                          <input type="text" id="postcode" class="form-control" name="kodepos" placeholder="kodepos">
                      </div>
                    </div>
                </div>





                <div class="form-group">
                    <label for="isi">Deskripsi Lengkap Laporan</label>
                    <textarea class="form-control summernote" id="isi" rows="5" name="isi"></textarea>
                </div>

                <div class="form-group">
                    <label for="thumbnail">Tambahkan Foto Lokasi</label>

                    <div class="row">
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail123">
                        </div>
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail[]">
                        </div>
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail[]">
                        </div>
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail[]">
                        </div>
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail[]">
                        </div>
                        <div class="col-md-2">
                            <input type="file" class="form-control" name="thumbnail[]">
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>

        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')
<script>
    $(document).ready(function() {

        $("#city-select").trigger("change");
          $("#kelurahan-select").trigger("change");
    });
    $("#city-select").on("change", function() {
        $("#kelurahan-select").val("");
        $("#kelurahan-select").select2({
            minimumResultsForSearch: -1,
            placeholder: "Pilih Kelurahan",

            containerCssClass: ":all:",
            ajax: {
                url: "/admin/kelurahan/" + $("#city-select").val(),
                dataType: "json",
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term,
                        kabupaten_id: $("#city-select").val()
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.kelurahan,
                                id: item.id
                            };

                        })
                    };
                },
                cache: true
            }
        });
    });
    $("#kelurahan-select").on("change", function() {
        $("#kecamatan-select").val("");
        $("#kecamatan-select").select2({
            minimumResultsForSearch: -1,
            placeholder: "Pilih Kecamatan",

            containerCssClass: ":all:",
            ajax: {
                url: "/admin/kecamatan/" + $("#kelurahan-select").val(),
                dataType: "json",
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term,
                        kelurahan_id: $("#kelurahan-select").val()
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {

                            return {
                                text: item.kecamatan,
                                id: item.id
                            };

                        })
                    };
                },
                cache: true
            }
        });
    });
    $("#kecamatan-select").on("change", function() {
      $.get("/admin/getpostcode/"+ $("#kecamatan-select").val(), function(data, status){

        document.getElementById("postcode").value=data;
   });


    });
    $('#kelurahan-select').select2({
        minimumResultsForSearch: -1
    });
    $('#kecamatan-select').select2({
        minimumResultsForSearch: -1
    });
</script>
@endpush
