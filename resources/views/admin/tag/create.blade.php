@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Buat Tag</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Buat Artikel
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="/admin/tag" method="POST" enctype="multipart/form-data" style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Tag</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="nama">Nama Tag</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Tag (contoh: berita)">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')

@endpush
