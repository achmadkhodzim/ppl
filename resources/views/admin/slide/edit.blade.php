@extends('layouts.admin')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit slide</h1>
        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-book fa-sm text-white-50"></i> Edit slide
            Baru</a> -->
    </div>

    <div class="card shadow mb-4">
        <form action="{{ url('/admin/slider/'.$slide->id) }}" method="POST" enctype="multipart/form-data" style="display:block">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">slide</h6>
            </div>
            <div class="card-body">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="judul" value="{{ $slide->judul }}">
                </div>


                <div class="form-group">
                    <label for="thumbnail">thumbnail</label>
                    <input type="file" class="form-control" id="thumbnail" name="thumbnail" data-height="300"
                        data-show-remove="false" data-default-file="{{ $slide -> thumbnail }}">
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control summernote" id="isi" rows="5" name="deskripsi">{{ $slide->deskripsi }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mb-5">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->
@endsection

@push('css')

@endpush

@push('javascript')

@endpush
