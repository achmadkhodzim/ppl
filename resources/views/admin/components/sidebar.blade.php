<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('admin/dashboard')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin Dash</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="{{url('admin/dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Blog
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseArtikel"
            aria-expanded="true" aria-controls="collapseArtikel">
            <i class="fas fa-fw fa-book"></i>
            <span>Laporan</span>
        </a>
        <div id="collapseArtikel" class="collapse" aria-labelledby="headingArtikel" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('admin/artikel/pilihlokasi')}}">Buat baru</a>
                <a class="collapse-item" href="{{url('admin/artikel')}}">semua berita</a>
            </div>
        </div>
    </li>
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseValidasi"
            aria-expanded="true" aria-controls="collapseValidasi">
            <i class="fas fa-fw fa-book"></i>
            <span>Validasi</span>
        </a>
        <div id="collapseValidasi" class="collapse" aria-labelledby="headingValidasi" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('admin/artikel/create')}}">Buat baru</a>
                <a class="collapse-item" href="{{url('admin/artikel')}}">semua berita</a>
            </div>
        </div>
    </li> -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTag" aria-expanded="true"
            aria-controls="collapseTag">
            <i class="fas fa-fw fa-tag"></i>
            <span>Tag</span>
        </a>
        <div id="collapseTag" class="collapse" aria-labelledby="headingTag" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('admin/tag/create')}}">Buat baru</a>
                <a class="collapse-item" href="{{url('admin/tag')}}">Semua Tag</a>
            </div>
        </div>
    </li> -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSlider" aria-expanded="true"
            aria-controls="collapseSlider">
            <i class="fas fa-fw fa-images"></i>
            <span>Slider</span>
        </a>
        <div id="collapseSlider" class="collapse" aria-labelledby="headingSlider" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{url('admin/slider/create')}}">Buat baru</a>
                <a class="collapse-item" href="{{url('admin/slider')}}">Semua Slider</a>
            </div>
        </div>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Akun
    </div>


    <li class="nav-item">
        <form class="nav-link" action="{{ route('logout') }}" method="POST">
            {{ csrf_field() }}

            <button type="submit" style="border:none;background:none;color:#ffffffcc;"><i class="fas fa-sign-out-alt"></i>
            <span>Logout</span></button>
        </form>

    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
