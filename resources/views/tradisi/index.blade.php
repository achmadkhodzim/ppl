@extends('tradisi.layout.master')
@section('slider')
<div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
</div>


<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{asset('images/bg-fix.png')}}" height="1140px" alt="First slide">
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="moving-text">
	<h2 class="moving-header" align="center" id="output">
		<iframe scrolling="no" frameborder="no" clocktype="html5" style="overflow:hidden;border:0;margin:0;padding:0;width:120px;height:40px;"
		  src="https://www.clocklink.com/html5embed.php?clock=043&amp;timezone=GMT0800&amp;color=yellow&amp;size=120&amp;Title=&amp;Message=&amp;Target=&amp;From=2020,1,1,0,0,0&amp;Color=yellow"></iframe>
	</h2>
	<marquee class="moving-body">
		Website Tugas PPL
	</marquee>
</div>

@endsection
@section('content')
<div class="container">
	<div class="row ">
		<div class="col-md-12  d-flex justify-content-center">
			<div class="jam">
				<iframe scrolling="no" frameborder="no" clocktype="html5" style="overflow:hidden;border:0;margin:0;padding:0;width:150px;height:150px;"
				  src="https://www.clocklink.com/html5embed.php?clock=005&amp;timezone=GMT0800&amp;color=blue&amp;size=150&amp;Title=&amp;Message=&amp;Target=&amp;From=2020,1,1,0,0,0&amp;Color=blue"></iframe>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12  d-flex justify-content-center">

			<h5>STATISTIK PENGUNJUNG :</h5>

		</div>
	</div>
	<div class="row">
		<div class="col-md-12  d-flex justify-content-center">

			<a href="https://www.hitwebcounter.com" target="_blank">
				<img src="https://hitwebcounter.com/counter/counter.php?page=7703373&amp;style=0030&amp;nbdigits=6&amp;type=page&amp;initCount=52" title="Total Website Hits" alt="Web Hits" border="0"></a>
			<br>
		</div>
	</div>
</div>


<div class="container-fluid">
  <label>
    Pilih Kabupaten
  </label>
  <select name="kota" id="filter-select">

    <option value="all"   @if($current=='all')
      selected
      @endif>Semua Kabupaten</option>
    @forelse($kabupaten as $kabupaten)
    <option value="{{$kabupaten->id}}"   @if($current==$kabupaten->id)
      selected
      @endif>{{$kabupaten->kabupaten}}</option>
    @empty

    @endforelse
  </select>
	<div class="row">
		@forelse ($articles as $artikel)
		<div class="col-md-3">
			<div class="card">
				<img class="card-img-top" src="{{asset($artikel->thumbnail)}}" alt="Card image cap" height="330px">
				<div class="card-body">
					<h5 class="card-title">{{$artikel->judul}}</h5>
					<p class="mb-md-5">
						@php
						$isi = strip_tags($artikel->isi);
						@endphp
						{{ Str::words($isi, $words = 50, $end = '...') }}
					</p>
					<p>
						<a href="{{ '/artikel/'. $artikel -> slug }}" class="btn btn-primary">Lihat Selengkapnya
							<span class="icon-arrow_forward ml-lg-4"></span></a>
					</p>
				</div>
			</div>
		</div>
		@empty
		@endforelse

	</div>

	<div class="row mt-5">
		<div class="col text-center  d-flex justify-content-center">
			<div class="block-27">
				<nav 	style="color:blue !important">
			{{		$articles->links('tradisi.layout.paginator')}}
</nav>


			</div>
		</div>
	</div>


</div>
</div>
@endsection
@push('javascript')
<script>

$("#filter-select").on("change", function() {
  window.location.href = "/home/"+$("#filter-select").val();
});
</script>
@endpush
