@extends('tradisi.layout.master')
@section('slider')
<div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
</div>


@endsection
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<img class="card-img-top" src="{{asset($artikel->thumbnail)}}" alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title">{{$artikel->judul}}</h5>
					<p class="mb-md-5">
						@php
						$isi = strip_tags($artikel->isi);
						@endphp
						{{ $isi }}
					</p>


				</div>
			</div>
		<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Bagikan</a></div>
		<a class="twitter-share-button"
	  href="https://twitter.com/intent/tweet?text={{$artikel->judul}}">
	Tweet</a>
	</div>



</div>
</div>
@endsection
