<div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
</div>

<div class="background">
  <img src="{{asset('images/bg-fix.png')}}" alt="Background" class="img-fluid" />
</div>

<div class="moving-text">
  <h2 class="moving-header" align="center" id="output">Selamat Datang</h2>
  <marquee class="moving-body">
    Website Resmi Sejarah dan Tradisi Bidang Kebudayaan dan Olahraga Dinas Pendidikan dan Kebudayaan Kota Balikpapan
  </marquee>
</div>
