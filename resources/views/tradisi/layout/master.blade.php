<!DOCTYPE html>
<html lang="en">
<style>
	.moving-text {
		width: auto;
		margin: 20px auto;
		box-shadow: 1px 1px 10px 2px #333;
		border-radius: 5px;
		overflow: hidden;
		color: black;
	}
	.text-pembukaan{
	padding-left: 50px;
	padding-top: 20px;
	}

	.tujuan{
	color: blue;
	padding-left:20px;
	padding-top:20px;
	}

	.program{
	color: blue;
	padding-left:20px;
	padding-top:20px;
	}
	.isi-sambutan{
	font-size:15pt;
	text-align: justify;
	color: black;
	padding-left: 50px;
	}

	.penutup{
	font-size:15pt;
	color:black;
	padding-left: 50px;
	}
	.moving-header {
		background: blue;
		color: white;
		padding: 15px 0px;
		text-transform: uppercase;
	}

	.moving-body {
		padding: 4px;
	}

	.background {
		display: flex;
		justify-content: center;
		align-items: center;
		overflow: hidden
	}

	.background img {
		flex-shrink: 0;
		min-width: 100%;
		min-height: 100%
	}
	.kontak{
	color: blue;
	padding-left: 20px;
	padding-top: 20px;
	}

	.kontak2{
	color: blue;
	padding-top: 20px;
	}

	.isi{
	color:blue;
	padding-left: 20px;
	padding-top: 20px;
	}

	.grid-container {
		display: grid;
		grid-gap: 10px 10px;
		grid-template-columns: auto auto auto;
		background-color: #ffffff;
		padding: 10px;
		justify-content: space-evenly;
		align-content: center;
		color: blue;
	}
	.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: blue !important;
}
.page-item.active .page-link {
    color: blue !important;
    background-color: gray !important;
		border-color: gray !important;
}
.page-link {
    color: blue !important;
}
</style>

<head>
	<title>BERANDA</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">

	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/jquery.fancybox.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">


	<link rel="stylesheet" href="{{asset('fonts/flaticon/font/flaticon.css')}}">

	<link rel="stylesheet" href="{{asset('css/aos.css')}}">

	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v9.0&appId=1155624254527525&autoLogAppEvents=1" nonce="mojWfmBi"></script>
</head>

<body>

	<div class="site-wrap">

		<div class="site-mobile-menu">
			<div class="site-mobile-menu-header">
				<div class="site-mobile-menu-close mt-3">
					<span class="icon-close2 js-menu-toggle"></span>
				</div>
			</div>
			<div class="site-mobile-menu-body"></div>
		</div> <!-- .site-mobile-menu -->


		<div class="site-navbar-wrap">

			<div class="site-navbar">
				<div class="container py-1">
					<div class="row align-items-center">
						<div class="col-2">
							<a href="{{url('/')}}"><img src="{{asset('images/sejtrad-logo-warna.png')}}" alt="Logo" height="80"></a>
						</div>
						<div class="col-10">
							<nav class="site-navigation text-right" role="navigation">
								<div class="container">
									<div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

									<ul class="site-menu js-clone-nav d-none d-lg-block">
										<li><a href="{{url('/')}}">Beranda</a></li>

									</ul>

									</ul>

									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>

		@yield('slider')
@yield('content')

		<footer class="site-footer">
			<div class="container">

				<div class="row pt-4 mt-5 text-center">
					<div class="col-md-12">
						<p>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright ©
						 Graybox	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
	<script src="{{asset('js/jquery-ui.js')}}"></script>
	<script src="{{asset('js/popper.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
	<script src="{{asset('js/jquery.countdown.min.js')}}"></script>
	<script src="{{asset('js/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('js/aos.js')}}"></script>

	<script src="{{asset('js/main.js')}}"></script>
	<script>

	</script>

	@stack('javascript')
</body>

</html>
