@extends('tradisi.layout.master')
@section('slider')
<div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
</div>


@endsection
@section('content')


<div class="grid-container">
    <div class="grid-item">
      <img src="{{asset('images/struktur.jpg')}}" alt="Struktur 1" class="img-fluid">
    </div>
  <div class="grid-item">
      <img src="{{asset('images/struktur2.png')}}" alt="Struktur 2" class="img-fluid">
    </div>
  <div class="grid-item">
      <img src="{{asset('images/visimisi.jpg')}}" alt="Visi Misi" class="img-fluid">
    </div>
  </div>

<div class="tujuan">
<h1>Tujuan dan Sasaran Jangka Menengah Dinas Pendidikan dan Kebudayaan</h1>
<h2>Misi</h2>
<p>Meningkatkan Sumber Daya Manusia yang Berkualitas dan Berdaya Saing Tinggi</p>

<h2>Tujuan</h2>
<p>Mewujudkan Sumber Daya Manusia yang cerdas intelektual, cerdas emosional, cerdas spiritual dan berdaya saing</p>

<h2>Sasaran</h2>
<p>1. Meningkatkan Kualitas Pendidikan.</p>
<p>2. Meningkatkan kualitas hidup dan kesejahteraan masyarakat secara menyeluruh dan berkelanjutan.</p>
<p>3.	Meningkatkan kenyamanan keamanan dan ketertiban lingkungan.</p>
</div>

<div class="program">
<h2>Arah Kebijakan</h2>
<p>1. Peningkatan Kualitas Pendidikan Non Formal Berbasis IPTEK dan Berlandaskan IMTAQ.</p>
<p>2. Perluasaan subsidi pendidikan bagi seluruh peserta didik usia sekolah.</p>
<p>3. Peningkatan kualitas dan kuantitas Tenaga Pendidik serta Tenaga Kependidikan.</p>
<p>4. Perluasan program penanggulangan kemiskinan.</p>
<p>5. Meningkatnya prestasi dan pelestarian kebudayaan daerah.</p>
<p>6. Meningkatkan pelestarian cagar budaya.</p>

<h2>Program Pembangunan</h2>
<p>1. Program Pendidikan Non Formal.</p>
<p>2.	Program Pendidikan Anak Usia Dini.</p>
<p>3.	Program Pendidikan Dasar.</p>
<p>4.	Program Pendidikan Menengah.</p>
<p>5.	Program Peningkatan Mutu Pendidik Dan Tenaga Kependidikan.</p>
<p>6.	Program Penanggulangan Kemiskinan Terpadu.</p>
<p>7.	Program Penggelolaan Keanekaragaman Budaya.</p>
<p>8.	Program Pelestarian Cagar Budaya.</p>
</div>


      </div>
    </div>
@endsection
