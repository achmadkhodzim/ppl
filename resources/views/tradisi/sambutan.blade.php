@extends('tradisi.layout.master')
@section('slider')
<div class="site-blocks-cover overlay" data-aos="fade" data-stellar-background-ratio="0.5">
</div>


@endsection
@section('content')

<div class="text-pembukaan">
<h4 style="font-weight: bold; color: black";>Sambutan Kepala Dinas Pendidikan dan Kebudayaan Kota Balikpapan</h4>
</div>

<div class="salam-pembuka" style="padding-left: 50px; padding-top: 30px; font-weight: bold;">
<h5 style="font-weight: bold; color: black";>Assalamualaikum Warohmatullahi Wabarokatuh</h5>
</div>

<div class="img-sambutan" style="padding-left: 50px; padding-top: 20px; margin-right 20px;">
<img src="{{asset('images/muhaimin.png')}}" alt="Kepala Dinas Pendidikan dan Kebudayaan Kota Balikpapan" style="float:left;">
</div>
<div class="isi-sambutan">
<p>
Puji dan syukur senantiasa kita panjatkan kehadirat Allah Subhanahu Wata’ala Tuhan Yang Maha Kuasa,
<br>
atas karunia-Nya kita senantiasa memilki kekuatan untuk berkarya menunaikan tugas bakti kepada nusa dan bangsa.
<br>
Sebagai ungkapan rasa bangga, saya memberikan apresiasi yang tinggi
<br>
atas keberadaan Website Seksi Sejarah dan Tradisi Dinas Pendidikan dan Kebudayaan Kota Balikpapan,
<br>
guna memberikan informasi yang sangat dibutuhkan kepada stakeholder dan masyarakat
<br>
terkait sanggar seni yang ada di Kota Balikpapan.
<br>
Dengan adanya Website ini diharapkan
dapat mempermudah pendaftaran dan registrasi ulang sanggar seni sekaligus
<br>
sebagai media informasi sanggar seni yang menjadi binaan Dinas Pendidikan dan Kebudayaan Kota Balikpapan
<br>
serta berbagai kegiatan dari Seksi Sejarah dan Tradisi.
Besar harapan saya Website ini terus eksis
<br>
dan selalu dikembangkan sesuai perkembangan era digital seperti saat ini.
<br>
Selamat berkarya untuk terus meningkatkan kualitas layanan kepada stakeholder dan masyarakat.
</p>
</div>

<div class="penutup">
<p>
Terimakasih,
<br>
Wassalamualaikum Warohmatullahi Wabarokatuh.
  <br>
<br>
Balikpapan, Nopember 2020
<br>
Kepala Dinas Pendidikan dan Kebudayaan
<br>
Kota Balikpapan,
<br>
<br>
MUHAIMIN
</p>

@endsection
